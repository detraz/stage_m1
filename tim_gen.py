#!/usr/bin/env python
# -*- coding: utf-8 -*-
#'''
# Basic fit from pulse in text input
# GPLv2

import subprocess
import psrchive
import heapq

stats = subprocess.check_output("psrstat -c freq *.tpf", cwd='/home/chirac/Stage_M1/Tutorial/archives', shell=True)

stats = stats.split('\n')
del stats[-1]


result = dict()

# dictionnary
for i in stats:
    res = i.split('freq=')
    if not result.has_key(float(res[1])):
        result[float(res[1])] = []
    result[float(res[1])].append(res[0])
    

print result
for freq in result:
    print freq

    if freq < 1500:
        temp_freq = 1398.0
    elif freq > 1700:
        temp_freq = 2048.0
    else:
        temp_freq = 1598.0

    chain = ''
    for name in result[freq]:
        chain += name

    print chain

    print "pat -f 'tempo2' -s addfile%s %s >> new.tim" % (temp_freq, chain)
    pat = subprocess.check_output("pat -f 'tempo2' -s addfile%s %s >> new.tim" % (freq, chain), cwd='/home/chirac/Stage_M1/Tutorial/archives', shell=True)
