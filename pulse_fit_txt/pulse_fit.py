#!/usr/bin/env python
# -*- coding: utf-8 -*-
#'''
# Basic fit from pulse in text input
# GPLv2

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.special import i0
import scipy.integrate as integrate
from scipy.signal import butter, lfilter, freqz
from numpy import sqrt
from scipy import asarray as ar,exp, fftpack, signal
import scipy
import os
import copy

####Def var

distrib = dict()
for i in range(0,7):
    distrib[i] = [] 

## Def filter

# Filtered signal
cuttoff_freq = 1.0
samp_rate = 20
norm_pass = cuttoff_freq/(samp_rate/2)
norm_stop = 1.5*norm_pass
(N, Wn) = signal.buttord(wp=norm_pass, ws=norm_stop, gpass=2, gstop=30, analog=0)
(b, a) = signal.butter(N, Wn, btype='low', analog=0, output='ba')



## Def functions


def gaussian(x, a, b, c):
    val = a * exp(-(x - b)**2 / c**2)
    return val

def von_misses(x, a, mu, kappa): 
    val = a * np.exp(kappa*np.cos(x-mu))/(2*np.pi*i0(kappa))
    return val 

def find_max(x2):
    if abs(min(x2)) > max(x2):
        abs_max = abs(min(x2))
        indice = x2.index(min(x2))
    else:
        abs_max = max(x2)
        indice = x2.index(max(x2))
    return abs_max, indice

## Gen graph gauss/von mises

def gen_graph_gaussian(data, indice_max):
    x1 = [i[2] for i in data]
    x2 = [i[3] for i in data]
    print max(x2)
    print min(x2)
    print x2.index(max(x2))
    print "plop"
    init_vals = [1, indice_max, 1] 
    popt, pcov = curve_fit(gaussian, x1, x2, p0=init_vals)
#    plt.plot(x1, gaussian(x1, popt[0], popt[1], popt[2]))
#    print("Scale =  %.3f +/- %.3f" % (popt[0], sqrt(pcov[0, 0])))
#    print("Offset = %.3f +/- %.3f" % (popt[1], sqrt(pcov[1, 1])))
#    print("Sigma =  %.3f +/- %.3f" % (popt[2], sqrt(pcov[2, 2])))
#    plt.plot(x1,x2)
    data_res = copy.copy(data)
    for i in data_res:
        i[3] = i[3] - gaussian(i[2], popt[0], popt[1], popt[2])
    return data_res, lambda x: gaussian(x, popt[0], popt[1], popt[2])

def gen_graph_vonmisses(data, indice_max):
    x1 = [i[2] for i in data]
    x2 = [i[3] for i in data]
    print max(x2)
    print min(x2)
    print x2.index(max(x2))
    print "plop"
    #init_vals = [1, indice_max, 1]
    init_vals = [50,50,1]
    popt, pcov = curve_fit(von_misses, x1, x2, p0=init_vals)
#    plt.plot(x1, gaussian(x1, popt[0], popt[1], popt[2]))
#    print("Scale =  %.3f +/- %.3f" % (popt[0], sqrt(pcov[0, 0])))
#    print("Offset = %.3f +/- %.3f" % (popt[1], sqrt(pcov[1, 1])))
#    print("Sigma =  %.3f +/- %.3f" % (popt[2], sqrt(pcov[2, 2])))
    plt.plot(x1,x2)
    data_res = copy.copy(data)
    for i in data_res:
        i[3] = i[3] - von_misses(i[2], popt[0], popt[1], popt[2])
    return data_res, lambda x: von_misses(x, popt[0], popt[1], popt[2])


### Genric fit loop

def fit_fct_fin(x, data):
    x1 = [i[2] for i in data]
    x2 = [i[3] for i in data]
    data_ex = data
    # Find creterium of stop loop
    seuil = max(np.abs(x2[0:150]))
    abs_max, indice = find_max(x2)
    # Loop
    fit_fct = 0
    while abs_max > seuil:
        data_ex, fit_fct_exp = gen_graph_gaussian(data_ex, indice)
#        data_ex, fit_fct_exp = gen_graph_vonmisses(data_ex, indice)
        fit_fct += fit_fct_exp(x)
        print "run"
        print abs_max
        x1 = [i[2] for i in data_ex]
        x2 = [i[3] for i in data_ex]
        abs_max, indice = find_max(x2)
    return fit_fct

#### Loop on files

for elem in os.listdir('/home/chirac/Stage_M1/pulse_fit_txt/pulsars_data'):
    if '.txt' in elem:
        # Open data and save data as data_or
	print str('/home/chirac/Stage_M1/pulse_fit_txt/pulsars_data/' + elem)
      	data = np.genfromtxt('/home/chirac/Stage_M1/pulse_fit_txt/pulsars_data/' + elem, dtype=None)
        data_or = data
        # Make colums of data
        x1 = [i[2] for i in data_or]
        x2 = [i[3] for i in data_or]
        # Final plot 
	print "plot"
        plt.plot(x1,x2)
        plt.plot(x1, fit_fct_fin(x1, data_or))
        plt.show()


	## Low pass filter
	flt = signal.lfilter(b, a, x2)
	plt.plot(x1, flt)
	plt.show()

	# Fourier transform
	# With data
	data_fft = np.fft.fft(x2)
        freq = np.fft.fftfreq(len(data_fft))
	plt.plot(freq,data_fft)
	# With fit
	fit_fft = np.fft.fft(fit_fct_fin(x1, data_or))
	plt.plot(freq, fit_fft)		
	plt.show()

        # Square signal
	plt.xlim(0,0.10)
	#plt.plot(freq, (abs(data_fft)**2))
	mod_car = np.conjugate(data_fft)*data_fft
 	plt.plot(freq, mod_car)
	print "Freq"
	print min(freq)
	print max(freq)

	# Make split in square signal
	len_mod = len(mod_car)
	for tranche in range(0,7):
	    print "MAX"
	    print max(mod_car[tranche*len_mod/80:(tranche+1)*len_mod/80])
	    print max(mod_car)/10
	    if max(mod_car[tranche*len_mod/80:(tranche+1)*len_mod/80]) > max(mod_car)/10:
		distrib[tranche].append(elem)
        plt.show()

	

	## Integrate
	#def integral(x):
        #    return integrate.quad(abs(data_fft)**2, 0, np.inf)
	#plt.plot(freq, integral(freq))	
	#plt.show()





print distrib
