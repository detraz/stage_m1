#!/usr/bin/env python
# -*- coding: utf-8 -*-
#'''
# Basic fit from pulse in text input
# GPLv2

from __future__ import division


import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.special import i0
from numpy import sqrt
from scipy.signal import butter, lfilter, freqz
from scipy import asarray as ar,exp, fftpack, signal
from scipy.integrate import simps
import os
import copy
import psrchive
import operator

def gen_filter():
    # Filtered signal
    cuttoff_freq = 7.0
    samp_rate = 20
    norm_pass = cuttoff_freq/(samp_rate/2)
    norm_stop = 1.5*norm_pass
    (N, Wn) = signal.buttord(wp=norm_pass, ws=norm_stop, gpass=2, gstop=30, analog=0)
    (b, a) = signal.butter(4, 1000000, btype='low', analog=True, output='ba')
    return (b, a)

def gaussian(x, a, b, c):
    val = a * exp(-(x - b)**2 / c**2)
    return val

def von_misses(x, a, mu, kappa): 
    val = a * np.exp(kappa*np.cos(x-mu))/(2*np.pi*i0(kappa))
    return val 

def find_max(x2):
    if abs(min(x2)) > max(x2):
        abs_max = abs(min(x2))
        indice = x2.index(min(x2))
    else:
        abs_max = max(x2)
        indice = x2.index(max(x2))
    return abs_max, indice

def gen_graph_gaussian(data, indice_max):
    #x1 = [i[2] for i in data]
    #x2 = [i[3] for i in data]
    x1 = data.get_data()[0][0][0]
    x2 = range(len(x1))
    print max(x2)
    print min(x2)
    print x2.index(max(x2))
    print "plop"
    init_vals = [1, indice_max, 1] 
    popt, pcov = curve_fit(gaussian, x1, x2, p0=init_vals)
#    plt.plot(x1, gaussian(x1, popt[0], popt[1], popt[2]))
#    print("Scale =  %.3f +/- %.3f" % (popt[0], sqrt(pcov[0, 0])))
#    print("Offset = %.3f +/- %.3f" % (popt[1], sqrt(pcov[1, 1])))
#    print("Sigma =  %.3f +/- %.3f" % (popt[2], sqrt(pcov[2, 2])))
    plt.plot(x2,x1)
    plt.show()
#    data_res = copy.copy(data)
    for i in x2:
        data.get_data()[0][0][0][i] = data.get_data()[0][0][0][i] - gaussian(i, popt[0], popt[1], popt[2])
    return data, lambda x: gaussian(x, popt[0], popt[1], popt[2])

def gen_graph_vonmisses(data, indice_max):
    x1 = [i[2] for i in data]
    x2 = [i[3] for i in data]
    print max(x2)
    print min(x2)
    print x2.index(max(x2))
    print "plop"
    init_vals = [1, indice_max, 1]
    popt, pcov = curve_fit(von_misses, x1, x2, p0=init_vals)
#    plt.plot(x1, gaussian(x1, popt[0], popt[1], popt[2]))
#    print("Scale =  %.3f +/- %.3f" % (popt[0], sqrt(pcov[0, 0])))
#    print("Offset = %.3f +/- %.3f" % (popt[1], sqrt(pcov[1, 1])))
#    print("Sigma =  %.3f +/- %.3f" % (popt[2], sqrt(pcov[2, 2])))
    plt.plot(x1,x2)
    data_res = copy.copy(data)
    for i in data_res:
        i[3] = i[3] - von_misses(i[2], popt[0], popt[1], popt[2])
    return data_res, von_misses(x1, popt[0], popt[1], popt[2])

def fit_fct_fin(x, data):
    #x1 = [i[2] for i in data]
    #x2 = [i[3] for i in data]
    x1 = data_or.get_data()[0][0][0]
    x2 = range(len(x1))
    data_ex = data
    # Find creterium of stop loop
    seuil = max(np.abs(x2[0:150]))
    abs_max, indice = find_max(x1)
    # Loop
    fit_fct = 0
    while abs_max > seuil:
        data_ex, fit_fct_exp = gen_graph_gaussian(data_ex, indice)
#            data_ex, fit_fct_exp = gen_graph_vonmisses(data_ex, indice)
        fit_fct += fit_fct_exp(x)
        print "run"
        print abs_max
        #x1 = [i[2] for i in data_ex]
        #x2 = [i[3] for i in data_ex]
        x1 = data_or.get_data()[0][0][0]
        x2 = range(len(x1))
        abs_max, indice = find_max(x2)
    return fit_fct




def gen_result(verbose=False):
    result = dict()
    result['pulsar_name']= []
    result['period'] = []
    #result['freq_max'] = []
    #result['freq_deux'] = []
    #result['freq_dix'] = []
    result['freq_distrib'] = dict()
    #result['final'] = dict()
    #for tranche in range(1,200):
    #    result[tranche] = []
        

    #for elem in os.listdir('/home/chirac/Stage_M1/scrunched/J2322+2057/P200-3'):
    for elem in os.listdir('/home/chirac/Stage_M1/scrunched/files'):
        # Open data and save data as data_or
	print str('templates/' + elem)
        #data = psrchive.Archive_load('/home/chirac/Stage_M1/scrunched/J2322+2057/P200-3/' + elem)
        data = psrchive.Archive_load('/home/chirac/Stage_M1/scrunched/files/' + elem)

        # On récupère les données
        center_freq = data.get_centre_frequency()
        integ = data.get_Integration(0)        
        period = integ.get_folding_period()
        pulsar_name = data.get_source()
        prof = data.get_Profile(0,0,0)
        snr = prof.snr()

        #On trie
        print(snr)
        if snr < 14:
            print("Decision = REJECT")
            continue
        print("Decision = ACCEPT")

        print center_freq
        data_or = data
        # Make colums of data
        x2 = data_or.get_data()[0][0][0]
        x2 = x2 - sum(x2)/len(x2)
        x1 = range(len(x2))
        # Final plot
        #print x1
        #print x2 
	#print "plot"
        ############ To  get signal
        #plt.plot(x1,x2)
        ############ To get fitted signal
        #plt.plot(x1, fit_fct_fin(x1, data_or))
        #plt.show()

        ## Low pass filter
        #flt = signal.lfilter(b, a, x2)
        #plt.plot(x1, flt)
        #plt.show()

	# Fourier transform
	# With data
    	data_fft = np.fft.fft(x2)
        freq = np.fft.fftfreq(len(data_fft))
	########## To get fft signal
        #plt.plot(freq,data_fft)
	# With fit
	#fit_fft = np.fft.fft(fit_fct_fin(x1, data_or))
	#plt.plot(freq, fit_fft)	
	#plt.show()

        # Square signal
        #plt.plot(freq, (abs(data_fft)**2))
        mod_car = np.conjugate(data_fft)*data_fft
        mask = freq >=0
        mod_car = mod_car[mask]
        freq = freq[mask]
        #plt.plot(freq, mod_car)
        #plt.show()



        if snr < 10 and verbose:
            plt.plot(x1,x2)
            plt.show() 

            plt.xlim(0,0.5)
            plt.plot(freq, mod_car)
            plt.show()
            #print "Freq"
            #print min(freq)
            #print max(freq)

        # On sauvegarde les données interressantes
        if not pulsar_name in result['pulsar_name']:
            result['pulsar_name'].append(pulsar_name)
            result['period'].append(period)
        #result['period'].append(period)
        max_ind = np.where(mod_car==max(mod_car))[0][0]
        #print max_ind
        #print mod_car[max_ind]
        #print max(mod_car)
        #result['freq_max'].append(freq[max_ind])
        # Calcul de l'intégrale
        print max(mod_car)
        print "MAX"
        integra = simps(mod_car, freq)
        for tranche in range(1,200):
            mask = ( freq < (0.5*tranche)/200 ) & (freq > (0.5*(tranche-1))/200 )
            mod_car_tr = mod_car[mask]
            tr_moy = sum(mod_car_tr)/len(mod_car_tr)
            print tr_moy
            if tr_moy > max(mod_car)/5:
                result.setdefault(pulsar_name, dict())
                result[pulsar_name][snr] = (0.5*tranche)/200
                result['freq_distrib'][elem] = (0.5*tranche)/200

    result_fin = dict()
    result_fin['filtered_data'] = []
    result_fin['filtered_period'] = []   
    for indice, pulsar in enumerate(result['pulsar_name']):
#        snr_max = max(result[pulsar].keys())
        for res in result[pulsar].keys():
        #result_fin['filtered_period'].append(result['period'][indice])
        #result_fin['filtered_data'].append(result[pulsar][snr_max])
            result_fin['filtered_period'].append(result['period'][indice])
            result_fin['filtered_data'].append(result[pulsar][res])
    result_fin['filtered_data2'] = result['freq_distrib'].values()
    return result_fin

if __name__ == '__main__':
    result = gen_result()
    print result
    f1 = plt.figure(1)
    #f2 = plt.figure(2)
    ax1 = f1.add_subplot(111)
    ax1.scatter(np.log10(result['period']),result['freq_distrib'].values(), color='red')
    #ax1.scatter(np.log10(result['period']),result['freq_max'], color='red')
    #ax1.scatter(np.log10(result['period']),result[1], color='blue')
    #ax2 = f2.add_subplot(111)
    #ax2.scatter(np.log10(result['period']),result['freq_dix'], color='green')
    plt.show()
