#!/usr/bin/env python
# -*- coding: utf-8 -*-
#'''
# Basic fit from pulse in text input
# GPLv2

import subprocess
import psrchive
import heapq

stats = subprocess.check_output("psrstat -c snr -c freq -c bw *.tpf4", cwd='/home/chirac/Stage_M1/Tutorial/archives', shell=True)

stats = stats.split('\n')
del stats[-1]


result = dict()

# dictionnary
for i in stats:
    line_splbw = i.split('bw=')
    if int(line_splbw[1]) != 128:
        continue  
    line_spl = line_splbw[0].split('freq=')
    if not result.has_key(float(line_spl[1])):
        result[float(line_spl[1])] = {}
    result[float(line_spl[1])][float(line_spl[0].split('snr=')[1])] = line_spl[0].split('snr=')[0]

print result
for freq in result:
    print freq
    print len(result[freq])

# 10 largest values
for freq in result:
    largest = {key: result[freq][key] for key in heapq.nlargest(10, result[freq].keys()) }

    addcommand  = ''
    for value in largest:
        addcommand += ' ' + largest[value]

    print addcommand
    print "psradd %s -o addfile%s" % (addcommand, freq)

    add = subprocess.check_output("psradd %s -o addfile4-%s" % (addcommand, freq), cwd='/home/chirac/Stage_M1/Tutorial/archives', shell=True)

#    for value in largest:
#        print "pat -s addfile%s %s > %s" % (freq, largest[value], largest[value].replace("tpf", "tim"))
#        pat = subprocess.check_output("pat -s addfile%s %s > %s" % (freq, largest[value], largest[value].replace("tpf", "tim")), cwd='/home/chirac/Stage_M1/Tutorial/archives', shell=True)
